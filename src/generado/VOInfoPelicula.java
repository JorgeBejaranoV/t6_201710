package generado;

public class VOInfoPelicula {

	private String titulo;
	private int anio;
	private String director;
	private String actores;
	private double ratingIMDB;
	
	public VOInfoPelicula(String pTitulo, int pAnio, String pDirector, String pActores, double pRatingIMDB){
		titulo=pTitulo;
		anio=pAnio;
		director=pDirector;
		actores=pActores;
		ratingIMDB=pRatingIMDB;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getActores() {
		return actores;
	}
	public void setActores(String actores) {
		this.actores = actores;
	}
	public double getRatingIMDB() {
		return ratingIMDB;
	}
	public void setRatingIMDB(double ratingIMDB) {
		this.ratingIMDB = ratingIMDB;
	}
	

}
