package generado;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Histogram {
	public static void main (String [] args){

        int num = 1;
        int[] nums = new int[10];  

        List<Integer> list = new ArrayList<Integer>();

        Scanner scan = new Scanner(System.in);


        while(num != 0){
           System.out.print("Enter a value to plot: ");
           num = scan.nextInt();
           System.out.println();
           if(num != 0)
              list.add(num);
        }


        for (int a = 0; a < list.size(); a++){
            nums[(list.get(a)-1) / 10]++;
        }

        for (int count = 0; count < 10; count++){
           System.out.print((count*1+1) + (count == 1 ? " ": " ") + "| \t");

           for(int h = 0; h < nums[count]; h++)
              System.out.print("#");

           System.out.println();
        }

    } // end of main
}
