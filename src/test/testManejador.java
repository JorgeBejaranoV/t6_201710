package test;

import estructuras.EncadenamientoSeparado;
import generado.VOInfoPelicula;
import junit.framework.TestCase;
import manejador.Manejador;

public class testManejador extends TestCase {
	private Manejador manejador;
	private final static int NUMERO_PELICULAS=9125;
	private void setupEscenario1(){
		manejador= new Manejador();
	}
	public void testCargar(){
		setupEscenario1();
		manejador.cargarPeliculas();
		EncadenamientoSeparado<Integer, VOInfoPelicula> tabla=manejador.getTabla();
		assertEquals((NUMERO_PELICULAS/2),tabla.darTamanio());
	}
	public void testGetAnio(){
		setupEscenario1();
		manejador.cargarPeliculas();
		int anio=manejador.getAnio(1);
		assertEquals(1995,anio);
		int anio2=manejador.getAnio(107);
		assertEquals(1996,anio2);
	
	}
	public void testGetTitulo(){
		setupEscenario1();
		manejador.cargarPeliculas();
		String anio=manejador.getTitulo(1);
		assertEquals("Toy Story",anio);
		String anio2=manejador.getTitulo(107);
		assertEquals("Muppet Treasure Island",anio2);
	}
	public void testGetActores(){
		setupEscenario1();
		manejador.cargarPeliculas();
		String anio=manejador.getActores(1);
		assertEquals("Tom Hanks, Tim Allen, Don Rickles, Jim Varney",anio);
		String anio2=manejador.getActores(107);
		assertEquals("Tim Curry, Billy Connolly, Jennifer Saunders, Kevin Bishop",anio2);
	
		
	}
	public void testGetRating(){
		setupEscenario1();
		manejador.cargarPeliculas();
		double anio=manejador.getRating(1);
		assertEquals(8.3,anio);
		double anio2=manejador.getRating(107);
		assertEquals(6.9,anio2);
	
	}
}
