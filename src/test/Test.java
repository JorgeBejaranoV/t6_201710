package test;
import estructuras.ListaLLaveValorSecuencial;
import junit.framework.TestCase;


public class Test extends TestCase {
	private ListaLLaveValorSecuencial<Integer, String> lista;
	private void setupEscenario1(){
		lista=new ListaLLaveValorSecuencial<Integer, String>();
		lista.insertar(0,"Falcao");
		lista.insertar(1,"Mi�ia");
		lista.insertar(2,"Teo");
	}
	private void setupEscenario2(){
		lista=new ListaLLaveValorSecuencial<Integer, String>();
	}
	private void setupEscenario3(){
		lista=new ListaLLaveValorSecuencial<Integer, String>();
		lista.insertar(0,"Duban");
	}

	public void testInsertar(){
		setupEscenario1();

		int numero=lista.darTamanio();
		lista.insertar(3,"Ospina");
		assertEquals( lista.darTamanio(),numero+1);

		lista.insertar(4,"John");
		assertEquals( lista.darTamanio(),numero+2);

		setupEscenario2();
		lista.insertar(0,"Insigne");
		assertEquals(lista.darTamanio(),1);

		setupEscenario3();
		lista.insertar(1,"Insigne");
		assertEquals(lista.darTamanio(),2);
	}
	public void testEstaVacia(){
		setupEscenario1();
		
		assertEquals(lista.estaVacia(),false);

		setupEscenario2();
		assertEquals(lista.estaVacia(),true);

		setupEscenario3();
		assertEquals(lista.estaVacia(),false);

	}
	public void testDarNumeroElementos(){
		setupEscenario1();

		
		assertEquals(lista.tieneLlave(1), true);

		setupEscenario2();
		assertEquals(lista.tieneLlave(0), false);

		setupEscenario3();
		assertEquals(lista.tieneLlave(0), true);

	}
	

}
