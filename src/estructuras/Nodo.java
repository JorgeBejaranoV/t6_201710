package estructuras;

public class Nodo <K,V> {

	private K  key;

	private V value;

	private Nodo<K, V> next;

	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}

	public Nodo<K, V> getNext() {
		return next;
	}

	public void setNext(Nodo<K, V> next) {
		this.next = next;
	}


}
